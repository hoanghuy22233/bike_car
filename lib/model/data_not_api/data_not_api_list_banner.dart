
class DataListBanner {
  final String img ;
  final int id;
  DataListBanner({
    this.img,
    this.id,
  });
}

List<DataListBanner> dataListBanner = [
  DataListBanner(
    img: "assets/images/logo_xe.jpg",
    id: 0, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListBanner(
    img: "assets/images/banner_car_three.jpg",
    id: 1, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListBanner(
    img: "assets/images/banner_car_two.jpg",
    id: 2, // imgDate:"assets/images/date_off_three.png",
  ),

];
