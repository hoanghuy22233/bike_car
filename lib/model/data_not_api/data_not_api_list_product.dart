
class DataListProduct {
  final String textForm,img ;
  final int id;
  DataListProduct({
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataListProduct> dataListProduct = [
  DataListProduct(
    textForm: "Tất cả",
    img: "assets/images/ic_grid.png",
    id: 0, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListProduct(
    textForm: "Xe tay ga",
    img: "assets/icons/ic_motorbiking_size_two.png",
    id: 1, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListProduct(
    textForm: "Xe số",
    img: "assets/icons/ic_car_number_size_two.png",
    id: 2, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListProduct(
    textForm: "Phân khối lớn",
    img: "assets/icons/ic_car_pkl_size_two.png",
    id: 3, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListProduct(
    textForm: "Xe đạp điện",
    img: "assets/icons/ic_bicycle.png",
    id: 4, // imgDate:"assets/images/date_off_three.png",
  ),
];
