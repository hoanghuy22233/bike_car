
class DataListPopularBrand {
  final String textForm,img ;
  final int id;
  DataListPopularBrand({
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataListPopularBrand> dataListPopularBrand = [
  DataListPopularBrand(
    textForm: "Honda",
    img: "assets/icons/ic_logo_honda_three.png",
    id: 0, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "Suzuki",
    img: "assets/icons/ic_logo_suzuki_three.png",
    id: 1, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "Yamaha",
    img: "assets/icons/ic_logo_yamaha_three.png",
    id: 2, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "Piaggio",
    img: "assets/icons/ic_logo_Piaggio_three.png",
    id: 3, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "BWM",
    img: "assets/icons/ic_logo_bmw.png",
    id: 4, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "Viettel",
    img: "assets/icons/ic_logo_viettell_three.png",
    id: 5, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListPopularBrand(
    textForm: "Mobifone",
    img: "assets/icons/ic_logo_mobifone_three.png",
    id: 6, // imgDate:"assets/images/date_off_three.png",
  ),



];
