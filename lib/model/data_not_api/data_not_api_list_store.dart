import 'package:flutter/cupertino.dart';

class ListStore {
  final String address;
  final String phone;
  final String website;
  final String email;

  ListStore({this.address, this.phone, this.website, this.email});

}
List<ListStore> listStore =[
  ListStore(
    address: "Shop xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    phone: "0888898000",
    website: "http://trumgop.com/",
    email: "abcxyz@gmail.com"
  )

];