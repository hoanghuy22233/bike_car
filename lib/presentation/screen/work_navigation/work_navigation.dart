import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/sc_profile_two.dart';

import 'package:bike_car/presentation/screen/menu/home/sc_home.dart';
import 'package:bike_car/presentation/screen/menu/news/sc_news.dart';
import 'package:bike_car/presentation/screen/menu/notification/sc_notification.dart';
import 'package:bike_car/presentation/screen/menu/store/sc_store.dart';
import 'package:bike_car/presentation/screen/work_navigation/widget_work_bottom_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bike_car/utils/handler/with_auth.dart';

class TabNavigatorRoutesWork {
//  static const String root = '/';
  static const String home = '/home';
  static const String news = '/news';
  static const String course = '/course';
  static const String contract = '/contract';
  static const String person = '/person';
}

class WorkNavigationScreen extends StatefulWidget {
  @override
  _WorkNavigationScreen createState() => _WorkNavigationScreen();
}

class _WorkNavigationScreen extends State<WorkNavigationScreen> {
  PageController _pageController;
  int id;

  List<WorkFABBottomNavItem> _navMenus = List();
  int _selectedIndex = 2;

  List<WorkFABBottomNavItem> _getTab() {
    return List.from([
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.news,
          tabItem: TabWorkItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/home.png',
          text: 'Trang chủ'),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.course,
          tabItem: TabWorkItem.course,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/online-course.png',
          text: "Cửa hàng"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.home,
          tabItem: TabWorkItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/new.png',
          text: "Tin tức"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.contract,
          tabItem: TabWorkItem.contract,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_bell.png',
          text: "Thông báo"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.person,
          tabItem: TabWorkItem.person,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/businessman.png',
          text: "Tài khoản"),
    ]);
  }

  goToPage({int page, int id = 0,}) {
    // WithAuth.isAuth(ifNotAuth: () {
    //   if (page == 4) {
    //     AppNavigator.navigateLogin();
    //   } else {
    //     if (page != _selectedIndex) {
    //       setState(() {
    //         this._selectedIndex = page;
    //       });
    //       _pageController.jumpToPage(_selectedIndex);
    //     }
    //   }
    // }, ifAuth: () {
    //   if (page != _selectedIndex) {
    //     setState(() {
    //       this._selectedIndex = page;
    //       //  this.homeDatas = homeDatas;
    //       //  this.id = id;
    //     });
    //     _pageController.jumpToPage(_selectedIndex);
    //   }
    // });
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
  }

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        // floatingActionButton: Container(
        //   width: 50,
        //   height: 50,
        //   margin: EdgeInsets.only(top: 0),
        //   child: FloatingActionButton(
        //     elevation: 4,
        //     shape:
        //         CircleBorder(side: BorderSide(color: Colors.white, width: 3)),
        //     onPressed: () {
        //       goToPage(page: 2);
        //     },
        //     child: FractionallySizedBox(
        //         widthFactor: 0.5,
        //         child: AspectRatio(
        //             aspectRatio: 1,
        //             child: Image.asset(
        //               'assets/icons/home.png',
        //               color: _selectedIndex == 2
        //                   ? AppColor.WORK_COLOR
        //                   : AppColor.NAV_ITEM_COLOR,
        //             ))),
        //     backgroundColor: Colors.white,
        //   ),
        // ),
        bottomNavigationBar: WorkWidgetFABBottomNav(
          notchedShape: CircularNotchedRectangle(),
          backgroundColor: Colors.grey[200],

          // backgroundColor: Colors.white,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          //  centerItemText: "A",
          items: _navMenus,
          selectedColor: AppColor.WORK_COLOR,
          color: Colors.grey,
          // color: AppColor.NAV_ITEM_COLOR,
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            HomePageScreen(),
            StorePageScreen(),
            NewsPageScreen(),
            NotificationPageScreen(),
            ProfileTwoPageScreen(),
          ],
        ));
  }
}
