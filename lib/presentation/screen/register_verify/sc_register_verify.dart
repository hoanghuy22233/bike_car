import 'package:bike_car/app/constants/color/color.dart';
import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/model/repo/user_repository.dart';
import 'package:bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:bike_car/presentation/screen/register_verify/register_verify_resend/bloc/register_verify_resend_bloc.dart';
import 'package:bike_car/presentation/screen/register_verify/widget_register_verify_appbar.dart';
import 'package:bike_car/presentation/screen/register_verify/widget_register_verify_form.dart';
import 'package:bike_car/presentation/screen/register_verify/widget_register_verify_username.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/register_verify_bloc.dart';

class  RegisterVerifyScreen extends StatefulWidget {
  @override
  _RegisterVerifyScreenState createState() => _RegisterVerifyScreenState();
}

class _RegisterVerifyScreenState extends State<RegisterVerifyScreen> {
  String _username;
  // String _phone;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        // _phone = arguments['phone'];
        print("_______________");
        print(_username);
        // print(_phone);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        top: true,
        child: Scaffold(
          backgroundColor: Color(0xff0bccd2),
          body: MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    RegisterVerifyBloc(userRepository: userRepository),
              ),
              BlocProvider(
                create: (context) =>
                    RegisterVerifyResendBloc(userRepository: userRepository),
              ),
            ],
            child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // _buildAppbar(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
                      child: GestureDetector(
                        onTap: () {
                          AppNavigator.navigateBack();
                        },
                        child: Image.asset(
                          "assets/images/ic_arrow.png",
                          width: 20,
                          height: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              WidgetSpacer(
                                height: 45,
                              ),
                              _buildUsername(),
                              _buildForm(),
                              // _buildResend(),
                              // WidgetSpacer(height: 20,),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetRegisterVerifyAppbar();

  _buildUsername() => WidgetRegisterVerifyUsername(
        username: _username,
      );

  _buildForm() => WidgetRegisterVerifyForm(
        username: _username,
        // phone: _phone,
      );
}
