
import 'package:bike_car/model/data_not_api/data_not_api_car_news.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListNewsCar extends StatelessWidget{
final int id;

  const ListNewsCar({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: Column(

          children: [
            Text("${dataCarNews[id].name}",maxLines: 2, overflow: TextOverflow.ellipsis, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Row(
              children: [
                Image.asset("${dataCarNews[id].featuredImage}", height: MediaQuery.of(context).size.height/6, width:MediaQuery.of(context).size.height/6 , fit: BoxFit.cover ,),
                Expanded(

                  child: Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Column(

                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${dataCarNews[id].details}",maxLines: 5, overflow: TextOverflow.ellipsis, style: TextStyle( fontSize: 14),),
                        Text("${dataCarNews[id].date} | ${dataCarNews[id].time}", style: TextStyle(color: Colors.grey, fontSize: 12),)
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }


}