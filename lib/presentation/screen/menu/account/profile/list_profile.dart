
import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/presentation/screen/menu/account/delivery_address/sc_delivery_address.dart';
import 'package:bike_car/presentation/screen/menu/account/delivery_address/sc_delivery_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListProfileTwo extends StatefulWidget {
  final int id;
  ChildListProfileTwo({Key key, this.id}) : super(key: key);

  @override
  _ChildListProfileTwoState createState() => _ChildListProfileTwoState();
}

class _ChildListProfileTwoState extends State<ChildListProfileTwo> {
  final double tabBarHeight = 80;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (dataListAccount[widget.id].id == 0 && widget.id == 0) {
          AppNavigator.navigateProfileDetail();
        }
        else if (dataListAccount[widget.id].id == 1 && widget.id == 1) {
          AppNavigator.deliveryAddress();
        }
        else if (dataListAccount[widget.id].id == 2 && widget.id == 2) {
          AppNavigator.navigateHistoryOder();
        }
         else if (dataListAccount[widget.id].id == 3 && widget.id == 3) {
           AppNavigator.navigateForgotPassword();
        }
        else if (dataListAccount[widget.id].id == 4 && widget.id == 4) {
          AppNavigator.navigateContact();
        }
        else if (dataListAccount[widget.id].id == 5 && widget.id == 5) {
          AppNavigator.navigateInstallment();
        }
        else if (dataListAccount[widget.id].id == 6 && widget.id == 6) {
          AppNavigator.navigateTerm();
        }
        else if (dataListAccount[widget.id].id == 7 && widget.id == 7) {
          AppNavigator.navigateLogin();
        }
        // else if (dataHome[widget.id].id == 2 && widget.id == 2) {
        //   return (
        //       showMaterialModalBottomSheet(
        //         shape: RoundedRectangleBorder(
        //           // borderRadius: BorderRadius.circular(10.0),
        //           borderRadius: BorderRadius.only(
        //               topLeft: Radius.circular(15.0),
        //               topRight: Radius.circular(15.0)),
        //         ),
        //         backgroundColor: Colors.white,
        //         context: context,
        //         builder: (context) => WorkPageScreen(),
        //
        //   ));
        // } else if (dataHome[widget.id].id == 3 && widget.id == 3) {
        //   return(
        //       showMaterialModalBottomSheet(
        //         shape: RoundedRectangleBorder(
        //           // borderRadius: BorderRadius.circular(10.0),
        //           borderRadius: BorderRadius.only(
        //               topLeft: Radius.circular(15.0),
        //               topRight: Radius.circular(15.0)),
        //         ),
        //         backgroundColor: Colors.white,
        //         context: context,
        //         builder: (context) => OfficePageScreen(),
        //       )
        //   );
        //   // return (Navigator.push(
        //   //   context,
        //   //   MaterialPageRoute(
        //   //       builder: (context) => SlidingUpPanel(
        //   //             controller: _panelController,
        //   //             maxHeight:
        //   //                 MediaQuery.of(context).size.height - tabBarHeight,
        //   //             body: HomePageScreen(),
        //   //             panelBuilder: (_scrollController) => OfficePageScreen(
        //   //               scrollController: _scrollController,
        //   //               // panelController: _panelController,
        //   //             ),
        //   //           )),
        //   // ));
        // } else if (dataHome[widget.id].id == 4 && widget.id == 4) {
        //   return (
        //       showMaterialModalBottomSheet(
        //         shape: RoundedRectangleBorder(
        //           // borderRadius: BorderRadius.circular(10.0),
        //           borderRadius: BorderRadius.only(
        //               topLeft: Radius.circular(15.0),
        //               topRight: Radius.circular(15.0)),
        //         ),
        //         backgroundColor: Colors.white,
        //           context: context,
        //           builder: (context) => CrmPageScreen(),
        //   ));
        // }
        // // else if (dataHome[widget.id].id == 5 && widget.id == 5) {
        // //   return (
        // //       Navigator.push(
        // //     context,
        // //     MaterialPageRoute(builder: (context) => HrmPageScreen()),
        // //   ));
        // // }
      },
      child:             Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Expanded(flex: 1,child: Container()),
            SizedBox(width: 5,),
            Expanded(flex: 1,child: Image.asset("${dataListAccount[widget.id].img}",width: 30,height: 30,)),
            SizedBox(width: 5,),
            Expanded(flex: 7,child: Text("${dataListAccount[widget.id].textForm}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),)),
            Expanded(flex: 2,child: Container()),
          ],
        ),
      ),

    );
  }
}
