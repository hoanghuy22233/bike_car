
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/list_profile.dart';
import 'package:flutter/material.dart';

class WidgetProfileForm extends StatefulWidget {
  @override
  _WidgetProfileFormState createState() => _WidgetProfileFormState();
}

class _WidgetProfileFormState extends State<WidgetProfileForm> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        child: Expanded(
            child: ListView.builder(
          itemCount: dataListAccount.length,
              itemBuilder: (context, index) => Container(
                height: MediaQuery.of(context).size.height/12,
                color: _selectedIndex != null && _selectedIndex == index
                    ? Colors.grey[200]
                    : Colors.white,
                // child: ChildListHomeTwo(
                //   id: index,
                // ),
                child: ListTile(
                  // title: Text("tets"),
                  title: ChildListProfileTwo(
                    id: index,
                  ),
                  onTap: () => _onSelected(index),
                ),
              ),

              // itemBuilder: (context, index) {
          //   return Container(
          //     width: MediaQuery.of(context).size.width,
          //     height: MediaQuery.of(context).size.height / 8,
          //     color: _selectedIndex != null && _selectedIndex == index
          //         ? Colors.grey[200]
          //         : Colors.white,
          //     // color: Colors.blue,
          //     margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
          //     child: ChildListHomeTwo(
          //       id: index,
          //     ),
          //   );
          // },
        )

            ),
      ),
    );
  }
}
