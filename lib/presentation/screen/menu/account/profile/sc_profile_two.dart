import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/app/constants/style/style.dart';
import 'package:bike_car/model/data_not_api/data_not_api_profile_customer.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile_two.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/widget_profile_form.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileTwoPageScreen extends StatefulWidget {
  @override
  _ProfileTwoPageScreenState createState() => _ProfileTwoPageScreenState();
}

class _ProfileTwoPageScreenState extends State<ProfileTwoPageScreen> {
  // int _selectedIndex = 0;
  // _onSelected(int index) {
  //   setState(() => _selectedIndex = index);
  // }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
        children: [
          Container(
            color: Color(0xff0bccd2),
            height: MediaQuery.of(context).size.height/4.8,
            child: Column(
              children: [
                _buildAppbar(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 80,
                        height: 80,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: Container(
                            decoration: BoxDecoration(
                                // color: Color(0xFFD6D5D5),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(500),
                                border: Border.all(color: Colors.white, width: 1)),
                            child: Padding(
                              padding: EdgeInsets.all(1.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(300.0),
                                child: Image.asset("assets/images/logo_xe.jpg",
                                    fit: BoxFit.cover
                                ),
                              ),
                              // child: AspectRatio(
                              //   aspectRatio: 1,
                              //   child: Image.asset(
                              //     'assets/images/img_nongnghiep.jpg',
                              //     fit: BoxFit.fitWidth,
                              //   ),
                              // ),
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              dataProfileCustomer[0].textForm,
                              style: const TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold,color: Colors.white),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${dataProfileCustomer[0].textPhone}",
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          SizedBox(
            height: 10,
          ),
          Expanded(child: WidgetProfileForm()),
        ],
      )),
    );
  }
}

_buildAppbar() => WidgetAppbarProfileTwo(
      backgroundColor: Color(0xff0bccd2),
      textColor: Colors.white,
      title: "Tài khoản",
      right: [
        // Padding(
        //   padding: EdgeInsets.only(top: 3, right: 10),
        //   child: GestureDetector(
        //     onTap: () {
        //       // showMaterialModalBottomSheet(
        //       //   shape: RoundedRectangleBorder(
        //       //     // borderRadius: BorderRadius.circular(10.0),
        //       //     borderRadius: BorderRadius.only(
        //       //         topLeft: Radius.circular(15.0),
        //       //         topRight: Radius.circular(15.0)),
        //       //   ),
        //       //   backgroundColor: Colors.white,
        //       //   context: context,
        //       //   builder: (context) => SeachWorkPageScreen(),
        //       // );
        //     },
        //     child: Image.asset(
        //       "assets/icons/nav_bell.png",
        //       width: 25,
        //       height: 25,
        //       color: Colors.white,
        //     ),
        //   ),
        // )
      ],
      left: [
        // Padding(
        //   padding: const EdgeInsets.only(left: 10),
        //   child: GestureDetector(
        //     onTap: () {
        //       AppNavigator.navigateBack();
        //     },
        //     // onTap: () {
        //     //   showMaterialModalBottomSheet(
        //     //     shape: RoundedRectangleBorder(
        //     //       // borderRadius: BorderRadius.circular(10.0),
        //     //       borderRadius: BorderRadius.only(
        //     //           topLeft: Radius.circular(15.0),
        //     //           topRight: Radius.circular(15.0)),
        //     //     ),
        //     //     backgroundColor: Colors.white,
        //     //     context: context,
        //     //     builder: (context) => SeachDateWorkPageScreen(),
        //     //   );
        //     // },
        //     child: Image.asset(
        //       "assets/images/ic_arrow.png",
        //       width: 20,
        //       height: 20,
        //       color: Colors.white,
        //     ),
        //   ),
        // )
      ],
    );

// Widget _buildAppbar() => WidgetProfileAppbar();
