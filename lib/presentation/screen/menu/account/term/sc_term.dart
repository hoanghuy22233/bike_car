import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/screen/menu/account/term/widget_term_appbar.dart';
import 'package:flutter/material.dart';


class TermScreen extends StatefulWidget {
  @override
  _TermScreenState createState() => _TermScreenState();
}

class _TermScreenState extends State<TermScreen>
    with AutomaticKeepAliveClientMixin<TermScreen> {
  @override
  void initState() {
    super.initState();
//    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  // var logger = Logger(
  //   printer: PrettyPrinter(),
  // );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
          ],
        ),
      ),
    );

  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "ĐIỀU KHOẢN",
    right: [
      // Padding(
      //   padding: EdgeInsets.only(top: 3, right: 10),
      //   child: GestureDetector(
      //     onTap: () {
      //       // showMaterialModalBottomSheet(
      //       //   shape: RoundedRectangleBorder(
      //       //     // borderRadius: BorderRadius.circular(10.0),
      //       //     borderRadius: BorderRadius.only(
      //       //         topLeft: Radius.circular(15.0),
      //       //         topRight: Radius.circular(15.0)),
      //       //   ),
      //       //   backgroundColor: Colors.white,
      //       //   context: context,
      //       //   builder: (context) => SeachWorkPageScreen(),
      //       // );
      //     },
      //     child: Image.asset(
      //       "assets/images/ic_fillter.png",
      //       width: 20,
      //       height: 20,
      //       color: Colors.white,
      //     ),
      //   ),
      // )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );


  Widget _buildMenu() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
        child: Column(
          children: [
            Text("Vui lòng đọc kỹ điều khoản trước khi sử dụng dịch vụ của chúng tôi",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold),),
            // HtmlWidget(
            //   state.appConfig.useOfTerms,
            // ),
            WidgetSpacer(
              height: 100,
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
