import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:bike_car/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InstallmentAppbar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
        child: WidgetAppbar(

          title: AppLocalizations.of(context).translate('installment_appbar.title'),

        )
    );

  }}