import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_certification.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_order_history.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

class HistoryOrderPageScreen extends StatefulWidget {
  @override
  _HistoryOrderPageScreenState createState() => _HistoryOrderPageScreenState();
}

class _HistoryOrderPageScreenState extends State<HistoryOrderPageScreen> {
  int id;
  String phone,name;
  // String _phone;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        id = arguments['id'];
        phone = arguments['phone'];
        name = arguments['name'];

        // _phone = arguments['phone'];
        print("_______________");
        print(id);
        // print(_phone);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(top: true,
        child: Column(
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView.builder(
                  itemCount: dataListOrderHistory.length,
                  itemBuilder: (context, index) {
                    return StickyHeader(
                      header: Container(
                        height: 50.0,
                        color: Color(0xff00b1b7),                        // decoration: BoxDecoration(
                        //   // color: Colors.blue,
                        //   border: Border(
                        //     top: BorderSide( //                   <--- left side
                        //       color: Colors.grey[400],
                        //       width: 2,
                        //     ),
                        //     bottom: BorderSide( //                    <--- top side
                        //       color: Colors.grey[400],
                        //       width: 2,
                        //     ),
                        //   ),
                        // ),
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        alignment: Alignment.centerLeft,
                        // child: Text('Header #$index',
                        //   style: const TextStyle(color: Colors.white),
                        // ),
                        child: Text(
                          "${dataListOrderHistory[index].textDateFull}",
                          style: const TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      content: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start ,
                          children: [
                            Row(
                              children: [
                                Text("Tên Xe: ",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("${dataListOrderHistory[index].textNameCar}",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text("Giá xe: ",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("${dataListOrderHistory[index].textPrice} đ",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                              ],
                            ),

                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text("Người đặt: ",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("${dataListOrderHistory[index].textName}",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),)
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text("Số điện thoại: ",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("${dataListOrderHistory[index].textPhone}",style: const TextStyle(
                                    color: Colors.blue, fontWeight: FontWeight.bold),)
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        )
    ));
  }
  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "LỊCH SỬ ĐẶT HÀNG",
    right: [
      // Padding(
      //   padding: EdgeInsets.only(top: 3, right: 10),
      //   child: GestureDetector(
      //     onTap: () {
      //       // showMaterialModalBottomSheet(
      //       //   shape: RoundedRectangleBorder(
      //       //     // borderRadius: BorderRadius.circular(10.0),
      //       //     borderRadius: BorderRadius.only(
      //       //         topLeft: Radius.circular(15.0),
      //       //         topRight: Radius.circular(15.0)),
      //       //   ),
      //       //   backgroundColor: Colors.white,
      //       //   context: context,
      //       //   builder: (context) => SeachWorkPageScreen(),
      //       // );
      //     },
      //     child: Image.asset(
      //       "assets/images/ic_fillter.png",
      //       width: 20,
      //       height: 20,
      //       color: Colors.white,
      //     ),
      //   ),
      // )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );

}
