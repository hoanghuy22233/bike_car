import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile_two.dart';
import 'package:bike_car/presentation/screen/menu/home/all_brands/widget_all_brands.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_banner.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_home.dart';
import 'package:bike_car/presentation/screen/menu/home/widget_certification.dart';
import 'package:bike_car/presentation/screen/menu/home/widget_news.dart';
import 'package:bike_car/presentation/screen/menu/home/widget_list_product.dart';
import 'package:bike_car/presentation/screen/menu/home/widget_popular_brand.dart';
import 'package:bike_car/presentation/screen/menu/home/widget_all_product.dart';
import 'package:bike_car/presentation/screen/menu/news/barrel_news..dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePageScreen extends StatefulWidget {
  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        child: Column(
          children: [
            Container(
              color: Color(0xff0bccd2),
              height: MediaQuery.of(context).size.height / 5,
              child: Column(
                children: [
                  _buildAppbar(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 8,
                          child: Container(
                            width: MediaQuery.of(context).size.width - 100,
                            decoration: BoxDecoration(
                              color: Color(0xff00b1b7),
                              borderRadius:
                              BorderRadius.all(Radius.circular(10.0)),
                            ),
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              // crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(flex: 2, child: Container()),
                                Image.asset(
                                  "assets/images/img_search.png",
                                  color: Colors.white,
                                  width: 15,
                                  height: 15,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  flex: 8,
                                  child: TextField(
                                    style: TextStyle(color: Colors.white),
                                    // keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        focusedBorder: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        border: InputBorder.none,
                                        hintText: 'Nhập từ khóa tìm kiếm',
                                        hintStyle:
                                        TextStyle(color: Colors.white)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Image.asset(
                              "assets/icons/notifications.png",
                              width: 25,
                              height: 25,
                              color: Colors.white,
                            ))
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child:  ListView(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 3,
                  child: Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return Image.asset(
                        "${dataListBanner[index].img}",
                        width: 100,
                        height: 100,
                        fit: BoxFit.fill,
                      );
                    },
                    autoplay: true,
                    pagination: new SwiperPagination(
                      alignment: Alignment.bottomCenter,
                      builder: new DotSwiperPaginationBuilder(
                          color: Colors.grey, activeColor: Colors.blue),
                    ),
                    itemCount: dataListBanner.length,
                    itemWidth: MediaQuery.of(context).size.width,
                    itemHeight: 400.0,
                    // layout: SwiperLayout.TINDER,
                    // control: new SwiperControl(
                    //     iconNext: null,
                    //     iconPrevious: null
                    // ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: WidgetListProduct(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 10,
                  color: Colors.grey[200],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 6,
                          child: Text(
                            "Thương hiệu phổ biến",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          )),
                      Expanded(flex: 1, child: Container()),
                      SizedBox(
                        width: 25,
                      ),
                      Expanded(
                          flex: 3,
                          child: GestureDetector(
                            onTap: () => Navigator.push(context,
                              MaterialPageRoute(builder: (context) => WidgetAllBrand()),),
                            child: Text(
                              "Xem tất cả",
                              style: TextStyle(
                                  color: Colors.lightBlue,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: WidgetPopularBrand(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 10,
                  color: Colors.grey[300],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Text(
                            "Chứng nhận bởi Gop 1",
                            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: WidgetCertification(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 10,
                  color: Colors.grey[300],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 6,
                          child: GestureDetector(
                            onTap: () => Navigator.push(context,
                              MaterialPageRoute(builder: (context) => NewsPageScreen()),),
                            child: Text(
                              "Tin mới nhất ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          )),
                      Expanded(flex: 1, child: Container()),
                      SizedBox(
                        width: 25,
                      ),
                      Expanded(
                          flex: 3,
                          child: Text(
                            "Xem thêm",
                            style: TextStyle(
                                color: Colors.lightBlue,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: WidgetNews(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 4,
                          child: Text(
                            "Tất cả sản phẩm",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          )),
                      // Expanded(flex: 1,child: Container() ),
                      SizedBox(width: 35,),
                      Text(
                        "Sắp xếp: ",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                          flex: 2,
                          child: Row(
                            children: [
                              Text(
                                "Tất cả",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(width: 3,),
                              Image.asset(
                                "assets/icons/ic_arrow_bottom.png",
                                width: 10,
                                height: 10,
                                color: Colors.black,
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: WidgetAllProduct(),
                ),

              ],
            ),)
          ],
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarHome(
        backgroundColor: Color(0xff0bccd2),
        textColor: Colors.white,
        title: "assets/images/logo_car_two.png",
        right: [
          // Padding(
          //   padding: EdgeInsets.only(top: 3, right: 10),
          //   child: GestureDetector(
          //     onTap: () {
          //       // showMaterialModalBottomSheet(
          //       //   shape: RoundedRectangleBorder(
          //       //     // borderRadius: BorderRadius.circular(10.0),
          //       //     borderRadius: BorderRadius.only(
          //       //         topLeft: Radius.circular(15.0),
          //       //         topRight: Radius.circular(15.0)),
          //       //   ),
          //       //   backgroundColor: Colors.white,
          //       //   context: context,
          //       //   builder: (context) => SeachWorkPageScreen(),
          //       // );
          //     },
          //     child: Image.asset(
          //       "assets/icons/nav_bell.png",
          //       width: 25,
          //       height: 25,
          //       color: Colors.white,
          //     ),
          //   ),
          // )
        ],
        left: [
          // Padding(
          //   padding: const EdgeInsets.only(left: 10),
          //   child: GestureDetector(
          //     onTap: () {
          //       AppNavigator.navigateBack();
          //     },
          //     // onTap: () {
          //     //   showMaterialModalBottomSheet(
          //     //     shape: RoundedRectangleBorder(
          //     //       // borderRadius: BorderRadius.circular(10.0),
          //     //       borderRadius: BorderRadius.only(
          //     //           topLeft: Radius.circular(15.0),
          //     //           topRight: Radius.circular(15.0)),
          //     //     ),
          //     //     backgroundColor: Colors.white,
          //     //     context: context,
          //     //     builder: (context) => SeachDateWorkPageScreen(),
          //     //   );
          //     // },
          //     child: Image.asset(
          //       "assets/images/ic_arrow.png",
          //       width: 20,
          //       height: 20,
          //       color: Colors.white,
          //     ),
          //   ),
          // )
        ],
      );
}
