import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_popular_brand.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_product.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/list_profile.dart';
import 'package:flutter/material.dart';

class WidgetPopularBrand extends StatefulWidget {
  @override
  _WidgetPopularBrandState createState() => _WidgetPopularBrandState();
}

class _WidgetPopularBrandState extends State<WidgetPopularBrand> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 7,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: dataListPopularBrand.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      // AppNavigator.navigateLogin();
                      _onSelected(index);
                    },
                    child:                           Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          color: _selectedIndex != null &&
                              _selectedIndex == index
                              ? Color(0xff96e5e7)
                              : Colors.grey[200],
                          // boxShadow: [
                          //   BoxShadow(color: Colors.green, spreadRadius: 5),
                          // ],
                          border: Border.all(  color: _selectedIndex != null &&
                              _selectedIndex == index
                              ? Colors.lightBlue
                              : Colors.grey[200],width: 2),
                          borderRadius:
                          BorderRadius.all(Radius.circular(5))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "${dataListPopularBrand[index].img}",
                            width: 55,
                            height: 55,
                            // color: _selectedIndex != null &&
                            //     _selectedIndex == index
                            //     ? Colors.blueAccent[600]
                            //     : Colors.grey[500],
                          ),
                          SizedBox(height: 5,),
                          Text("${dataListPopularBrand[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),

                  ),
                  SizedBox(
                    height: 10,
                  ),

                ],
              ),
            );
          }),
    );
  }
}
