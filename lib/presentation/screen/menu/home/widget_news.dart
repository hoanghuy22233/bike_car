import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_news.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_popular_brand.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_product.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/list_profile.dart';
import 'package:flutter/material.dart';

class WidgetNews extends StatefulWidget {
  @override
  _WidgetNewsState createState() => _WidgetNewsState();
}

class _WidgetNewsState extends State<WidgetNews> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 3,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: dataListNews.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[300],
                  ),
                  SizedBox(height: 10,),
                  GestureDetector(
                    onTap: () {
                      // AppNavigator.navigateLogin();
                    },
                    child: Row(
                      children: [
                        Container(
                          width: 80,
                          height: 30,
                          decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Center(
                            child: Text("Tin tức"),
                          ),
                        ),
                        SizedBox(width: 20,),
                        Text(dataListNews[index].textForm.length>30?'${dataListNews[index].textForm.substring(0, 30)}...' : dataListNews[index].textForm,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),)
                      ],
                    ),
                  ),
                  SizedBox(height: 10,),

                ],
              ),
            );
          }),
    );
  }
}
