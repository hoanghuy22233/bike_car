import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/app/constants/color/color.dart';
import 'package:bike_car/model/repo/user_repository.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:bike_car/presentation/screen/forgot_password/widget_bottom_login.dart';
import 'package:bike_car/presentation/screen/forgot_password/widget_forgot_password_appbar.dart';
import 'package:bike_car/presentation/screen/forgot_password/widget_forgot_password_form.dart';
import 'package:bike_car/presentation/screen/forgot_password/widget_forgot_password_title.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_bloc.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff0bccd2),
          body: BlocProvider(
            create: (context) =>
                ForgotPasswordBloc(userRepository: userRepository),
            child: Container(
                // color: AppColor.PRIMARY_BACKGROUND,
                child: Column(
                  children: [
                    _buildAppbar(),
                    Expanded(
                        child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 25),
                            child: Text("Quên Mật Khẩu",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white,fontFamily: 'Roboto'),),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          _buildForgotPasswordForm(),
                          _buildBottomLogin()
                        ],
                      ),
                    )),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff0bccd2),
    textColor: Colors.white,
    // title: "Công Việc",
    // right: [
    //   Padding(
    //     padding: EdgeInsets.only( top: 3,right: 10),
    //     child: GestureDetector(
    //       onTap: () {
    //         // showMaterialModalBottomSheet(
    //         //   shape: RoundedRectangleBorder(
    //         //     // borderRadius: BorderRadius.circular(10.0),
    //         //     borderRadius: BorderRadius.only(
    //         //         topLeft: Radius.circular(15.0),
    //         //         topRight: Radius.circular(15.0)),
    //         //   ),
    //         //   backgroundColor: Colors.white,
    //         //   context: context,
    //         //   builder: (context) => SeachWorkPageScreen(),
    //         // );
    //       },
    //       child: Image.asset(
    //         "assets/icons/phone-call.png",
    //         width: 25,
    //         height: 25,
    //         color: Colors.white,
    //       ),
    //     ),
    //   )
    // ],
    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          // onTap: () {
          //   showMaterialModalBottomSheet(
          //     shape: RoundedRectangleBorder(
          //       // borderRadius: BorderRadius.circular(10.0),
          //       borderRadius: BorderRadius.only(
          //           topLeft: Radius.circular(15.0),
          //           topRight: Radius.circular(15.0)),
          //     ),
          //     backgroundColor: Colors.white,
          //     context: context,
          //     builder: (context) => SeachDateWorkPageScreen(),
          //   );
          // },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );


  _buildForgotPasswordForm() => WidgetForgotPasswordForm();
  _buildBottomLogin() => WidgetBottomLogin();
}
