import 'package:bike_car/app/auth_bloc/bloc.dart';
import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/model/repo/barrel_repo.dart';
import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:bike_car/presentation/screen/login/widget_login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/login_bloc.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff0bccd2),
        body: SafeArea(
          top: true,
          child: Column(
            children: [
              Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    scrollDirection: Axis.vertical,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Text("Đăng nhập",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white,fontFamily: 'Roboto'),),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      _buildLoginForm(),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
  _buildLoginForm() => WidgetLoginForm();

}
