import 'package:bike_car/presentation/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigateBacks() async {
    var result = await Get.offNamed(BaseRouter.WORK_NAVIGATION);
    return result;
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }
  static navigateWorkService() async {
    var result = await Get.offAllNamed(BaseRouter.WORK_NAVIGATION);
    return result;
  }


  static popToNews() async {
    Get.until((route) {
      if (route.settings.name == BaseRouter.WORK_NAVIGATION) {
        return true;
      }
      return false;
    });
  }
  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD);
    return result;
  }
  static navigateForgotPasswordVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }
  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
    await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }
  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }
  static navigateRegisterVerify({String username}) async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY,
        arguments: {'username': username});
    return result;
  }
  static navigateProfileDetail() async {
    var result = await Get.toNamed(BaseRouter.PROFILE_DETAIL);
    return result;
  }
  static navigateTerm() async {
    var result = await Get.toNamed(BaseRouter.TERM);
    return result;
  }
  static navigateContact() {
    Get.toNamed(BaseRouter.CONTACT);
  }
  static navigateInstallment(){
    Get.toNamed(BaseRouter.INSTALLMENT);
  }
  static deliveryAddress() {
    Get.toNamed(BaseRouter.DELIVERY_ADDRESS);
  }
  static detailProduct({int id}) {
    Get.toNamed(BaseRouter.DETAIL_PRODUCT, arguments: {'id': id});
  }
  static navigateAddOder({int id}) {
    Get.toNamed(BaseRouter.ADD_ODER, arguments: {'id': id});
  }
  static navigateHistoryOder(
      // {int id,String phone, String name}
      ) {
      Get.toNamed(BaseRouter.HISTORY_ORDER,
          // arguments: {'id': id,'phone':phone,'name':name}
          );
  }
}
