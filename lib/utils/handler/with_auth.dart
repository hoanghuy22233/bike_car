import 'package:bike_car/app/auth_bloc/authentication_bloc.dart';
import 'package:bike_car/app/auth_bloc/authentication_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class WithAuth {
  static bool isAuth({Function ifAuth, Function ifNotAuth}) {
    if (BlocProvider.of<AuthenticationBloc>(Get.context).state
        is Authenticated) {
      ifAuth();
      return true;
    } else {
      ifNotAuth();
      return false;
    }
  }
}
